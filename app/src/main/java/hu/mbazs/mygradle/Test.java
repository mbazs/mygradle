package hu.mbazs.mygradle;

@lombok.Data
public class Test {
    private int number;
    private String str;

    public Test() {
        this.number = 123;
        this.str = "blah";
    }
}
